// *************************************
// //
// //   Gulpfile
// //
// // *************************************
// //
// //  Available tasks:
// //      `gulp`
// //      `gulp sass:watch`
// //
// // *************************************

var gulp = require('gulp');
var jsonImporter = require('node-sass-json-importer');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var util = require('gulp-util');

gulp.task('default', function(){
    var instructions =
    "gulp: available functions\n" +
        "\t\tgulp sass:watch" + "\n\t\t\t- makes css from scss files'";
    return util.log(instructions);
});


gulp.task('sass', function(){
    return gulp.src(["./app/sass/**/*.scss"])
    .pipe(sourcemaps.init())
    .pipe(sass({file: "_variables.scss", importer: jsonImporter}, true).on("error", sass.logError))
    .pipe(sass().on("error", sass.logError))
    .pipe(sourcemaps.write("../maps"))
    .pipe(gulp.dest("./app/css"));
});

gulp.task('sass:watch', function(){
 gulp.watch(["./app/sass/**/*.scss", "./app/json/**/*.json" ], ["sass"]);
});

