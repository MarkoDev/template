'use strict'

var app = angular.module('myApp', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider
    .state('home', {
        url: '/',
        templateUrl: 'shared/templates/home.html'
    })
    .state('news', {
        url: '/news',
        templateUrl: 'shared/templates/news.html'
    })
    .state('contact', {
        url: '/contact',
        templateUrl: 'shared/templates/contact.html'
    })
    .state('about', {
        url: '/about',
        templateUrl: 'shared/templates/about.html'   
    })
    .state('other.image', {
        url: '/image:id',//takodje moze '/image{id}  , a moze i image?:id'
        templateUrl: function (stateParams){
        return 'shared/templates/other-image' + stateParams.id + '.html';
       }     
    }) 
    .state('other', {
        url: '/other',
        templateUrl: 'shared/templates/other.html'
    });


})
.controller('rootController', ['$scope', '$state', function($scope, $state) {
    $scope.menuItems = [
        {name: 'home'},
        {name: 'news'},
        {name: 'contact'},
        {name: 'about'},
        {name: 'other'}
    ];

    $scope.$on('$stateChangeSuccess', 
        function(event, toState, toParams, fromState, fromParams, options){
            $scope.state = $state.current;
        });


}
]);   
app.controller('formCtrl', function($scope) {
    $scope.name = [];
    $scope.email = [];
    $scope.message = [];
});